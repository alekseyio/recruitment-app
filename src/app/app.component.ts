import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

import { StorageService } from './services/storage.service';
import { AuthService } from './auth/services/auth.service';
import { NotificationService } from './notifications/notification.service';
import { User } from './auth/interfaces/User.interface';
import { GlobalLoaderService } from './services/global-loader.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  constructor(
    private router: Router,
    private storageService: StorageService,
    private authService: AuthService,
    private notificationService: NotificationService,
    private loaderService: GlobalLoaderService
  ) {}

  ngOnInit() {
    this.loaderService.show();

    const token: string = this.storageService.get('token');

    if (!token) {
      this.loaderService.hide();
      return this.router.navigateByUrl('/auth/login');
    }

    this.authService
      .authenticate(token)
      .subscribe(this.handleAuth.bind(this), this.handleAuthError.bind(this));
  }

  private handleAuth(response: { user: User }): void {
    this.loaderService.hide();
    this.authService.setUser(response.user);
    this.notificationService.show('Вы успешно авторизовались');
  }

  private handleAuthError(error: HttpErrorResponse): void {
    if (error.status === 400) {
      this.loaderService.hide();
      this.notificationService.show(
        'Сессия истекла. Войдите в учётную запись снова'
      );
      this.storageService.clear('token');

      this.router.navigateByUrl('/auth/login');
    } else {
      this.loaderService.hide();
      this.notificationService.show(
        'Ошибка авторизации. Сервер не ответил на запрос.'
      );
      this.router.navigateByUrl('/server-offline');
    }
  }
}
