import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { FormValues } from '../interfaces/FormValues.interface';
import { RegisterService } from '../services/register.service';
import { ValidationError } from '../interfaces/ValidationError.interface';
import { NotificationService } from '../../notifications/notification.service';

interface Role {
  name: string;
  value: string;
}

@Component({
  selector: 'app-auth-login',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  providers: [RegisterService]
})
export class RegisterComponent implements OnInit {
  readonly roles: Role[] = [
    { name: 'Директор', value: 'director' },
    { name: 'Рекрутер', value: 'recruiter' }
  ];
  public form: FormGroup;
  public errors: ValidationError;
  public isLoading = false;

  constructor(
    private registerService: RegisterService,
    private router: Router,
    private notificationService: NotificationService
  ) {}

  ngOnInit() {
    this.initForm();
    this.initErrors();
  }

  private initForm(): void {
    this.form = new FormGroup({
      firstName: new FormControl(''),
      lastName: new FormControl(''),
      middleName: new FormControl(''),
      email: new FormControl(''),
      password: new FormControl(''),
      passwordConfirmation: new FormControl(''),
      role: new FormControl('')
    });
  }

  private initErrors(): void {
    this.errors = {
      firstName: null,
      lastName: null,
      middleName: null,
      email: null,
      password: null,
      passwordConfirmation: null,
      role: null
    };
  }

  public handleSubmit(): void {
    this.isLoading = true;

    const values = this.form.value as FormValues;

    this.registerService
      .register(values)
      .subscribe(
        this.handleRegister.bind(this),
        this.handleRegisterError.bind(this)
      );
  }

  private handleRegister(): void {
    this.isLoading = false;

    this.notificationService.show(
      'Регистрация прошла успешно. Теперь вы можете войти'
    );
    this.router.navigateByUrl('/auth/login');
  }

  private handleRegisterError(errors: ValidationError): void {
    this.isLoading = false;
    this.errors = errors;
  }
}
