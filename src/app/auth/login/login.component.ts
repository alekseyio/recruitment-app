import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { StorageService } from '../../services/storage.service';
import { ValidationError } from '../interfaces/ValidationError.interface';
import { User } from '../interfaces/User.interface';
import { LoginService } from '../services/login.service';
import { AuthService } from '../services/auth.service';
import { NotificationService } from '../../notifications/notification.service';

interface FormValues {
  email: string;
  password: string;
}

@Component({
  selector: 'app-auth-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [LoginService]
})
export class LoginComponent implements OnInit {
  public form: FormGroup;
  public errors: ValidationError;
  public isLoading = false;

  constructor(
    private loginService: LoginService,
    private authService: AuthService,
    private storageService: StorageService,
    private router: Router,
    private notificationService: NotificationService
  ) {}

  ngOnInit() {
    this.initForm();
    this.initErrors();
  }

  private initForm(): void {
    this.form = new FormGroup({
      email: new FormControl(''),
      password: new FormControl('')
    });
  }

  private initErrors(): void {
    this.errors = {
      email: null,
      password: null
    };
  }

  public handleSubmit(): void {
    this.initErrors();
    this.isLoading = true;

    const values = this.form.value as FormValues;

    this.loginService
      .login(values.email, values.password)
      .subscribe(
        this.handleLoginSuccess.bind(this),
        this.handleLoginError.bind(this)
      );
  }

  private handleLoginSuccess(response: { token: string; user: User }): void {
    this.isLoading = false;

    this.authService.setUser(response.user);
    this.storageService.set('token', response.token);
    this.notificationService.show('Вы успешно авторизовались');

    this.router.navigateByUrl('/');
  }

  private handleLoginError(errors: ValidationError): void {
    this.isLoading = false;
    this.errors = errors;
  }
}
