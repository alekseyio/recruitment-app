interface Company {
  brand: string;
  objectNumber: number;
  subway: string;
  subwayLine: number;
  address: string;
}

export interface User {
  id: string;
  firstName: string;
  lastName: string;
  middleName: string;
  email: string;
  role: string;
  company: Company | null | undefined;
}
