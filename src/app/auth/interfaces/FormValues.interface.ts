export interface FormValues {
  firstName: string;
  lastName: string;
  middleName: string;
  email: string;
  password: string;
  passwordConfirmation: string;
  role: string;
}
