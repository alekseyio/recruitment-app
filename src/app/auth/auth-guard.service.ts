import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { map, delay } from 'rxjs/operators';

import { AuthService } from './services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  canActivate() {
    return this.authService.authenticated$.pipe(
      delay(100),
      map(authenticated => {
        if (!authenticated) {
          return true;
        }

        return this.router.createUrlTree(['/']);
      })
    );
  }
}
