import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { User } from '../interfaces/User.interface';
import { ValidationError } from '../interfaces/ValidationError.interface';
import { NotificationService } from '../../notifications/notification.service';

@Injectable()
export class LoginService {
  private url = '/auth/login';

  constructor(
    private http: HttpClient,
    private notificationService: NotificationService
  ) {}

  public login(email: string, password: string) {
    return this.http.post(this.url, { email, password }).pipe(
      map(this.handleLogin),
      catchError(this.handleLoginError.bind(this))
    );
  }

  private handleLogin(response: {
    token: string;
    expires: number;
    user: User;
  }): { token: string; user: User } {
    return {
      token: response.token,
      user: response.user
    };
  }

  private handleLoginError(
    err: HttpErrorResponse
  ): Observable<ValidationError> {
    let errors: ValidationError = {};

    if (err.status === 400) {
      errors = err.error.errors;
    } else {
      this.notificationService.show(
        'Произошла ошибка на стороне сервера. Попробуйте позднее'
      );
    }

    return throwError(errors);
  }
}
