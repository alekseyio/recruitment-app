import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';

import { User } from '../interfaces/User.interface';
import { StorageService } from '../../services/storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly currentUserURL = '/auth/current';
  private _user = new BehaviorSubject<User>(null);
  private _authenticated = new BehaviorSubject<boolean>(false);
  public user$ = this._user.asObservable();
  public authenticated$ = this._authenticated.asObservable();

  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {}

  private get user(): User {
    return this._user.getValue();
  }

  private set user(user: User) {
    if (user !== null) {
      this._user.next(user);
      this._authenticated.next(true);
    } else {
      this._user.next(user);
      this._authenticated.next(false);
    }
  }

  public setUser(user: User): void {
    this.user = user;
  }

  public getUser(): User {
    return this.user;
  }

  public authenticate(token: string): Observable<{ user: User }> {
    const headers = new HttpHeaders({ Authorization: token });

    return this.http.get<{ user: User }>(this.currentUserURL, { headers });
  }

  public fetchUser(): void {
    const token = this.storageService.get('token');
    const headers = new HttpHeaders({ Authorization: token });

    this.http
      .get(this.currentUserURL, { headers })
      .subscribe((response: { user: User }) => (this.user = response.user));
  }
}
