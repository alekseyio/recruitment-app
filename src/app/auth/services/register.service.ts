import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { FormValues } from '../interfaces/FormValues.interface';
import { ValidationError } from '../interfaces/ValidationError.interface';
import { NotificationService } from '../../notifications/notification.service';

@Injectable()
export class RegisterService {
  private url = '/auth/register';

  constructor(
    private http: HttpClient,
    private notificationService: NotificationService
  ) {}

  public register(values: FormValues) {
    return this.http
      .post(this.url, values)
      .pipe(catchError(this.handleRegisterError));
  }

  private handleRegisterError(err: HttpErrorResponse) {
    let errors: ValidationError = {};

    if (err.status === 400) {
      errors = err.error.errors;
    } else {
      this.notificationService.show(
        'Произошла ошибка на стороне сервера. Попробуйте позднее'
      );
    }

    return throwError(errors);
  }
}
