import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
} from '@angular/common/http';

import { environment } from '../environments/environment';

@Injectable()
export class UrlPrefixInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler) {
    let url = 'http://127.0.0.1:5000/api';
    // let url = 'https://meetupbe.herokuapp.com/api';

    if (environment.production) {
      url = 'https://meetupbe.herokuapp.com/api';
    }

    const newReq = req.clone({
      url: `${url}${req.url}`,
    });

    return next.handle(newReq);
  }
}
