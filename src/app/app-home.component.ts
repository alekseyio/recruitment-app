import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { AuthService } from './auth/services/auth.service';

@Component({
  selector: 'app-home',
  template: `
    <router-outlet></router-outlet>
    <app-notification></app-notification>
  `
})
export class AppHomeComponent implements OnInit, OnDestroy {
  private authSubscription: Subscription;

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit() {
    this.authSubscription = this.authService.user$.subscribe(user => {
      if (user) {
        this.navigateAway(user.role);
      }
    });
  }

  ngOnDestroy() {
    this.authSubscription.unsubscribe();
  }

  private navigateAway(role: string): void {
    let url = '/not-found';

    if (role === 'director') {
      url = '/director';
    } else if (role === 'recruiter') {
      url = '/recruiter';
    }

    this.router.navigateByUrl(url);
  }
}
