import { IRequest } from './Request.interface';
import { IPerson } from './Person.interface';

export interface IInterview {
  id: string;
  request: IRequest;
  person: IPerson;
  date: string;
  active: boolean;
  result?: string;
  comment?: string;
}
