export interface IRequest {
  id: string;
  _id?: string;
  title: string;
  position: string;
  schedule: string;
  rate: number;
  qty: string;
  comment: string;
  status: string;
  user: string;
}
