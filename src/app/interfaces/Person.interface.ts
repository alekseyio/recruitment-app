export interface IPersonBlank {
  lastName: string;
  firstName: string;
  middleName: string;
  age: number;
  citizenship: string;
  phone: string;
  comment: string;
  employed: boolean;
  resource: string;
}

export interface IEmployedPersonBlank extends IPersonBlank {
  job: {
    company: string;
    position: string;
    schedule: string;
    rate: number;
  };
}

export interface IPerson extends IPersonBlank {
  id: string;
  _id?: string;
}

export interface IEmployedPerson extends IPerson, IEmployedPersonBlank {}
