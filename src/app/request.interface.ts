import { User } from './auth/interfaces/User.interface';
import { Person } from './recruiter/database/person.interface';

export interface Interview {
  _id: string;
  person: Person;
  interviewDate: Date;
  result?: string;
  comment?: string;
}

export interface Request {
  id: string;
  title: string;
  position: string;
  rate: number;
  schedule: string;
  qty: number;
  comment?: string;
  status: string;
  user: User;
  interviews?: Interview[];
  createdAt: string;
}
