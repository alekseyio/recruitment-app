import { Component, OnInit, OnDestroy } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';
import { Subscription } from 'rxjs';

import { NotificationService } from '../notification.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({ transform: 'translate(140%, 0)' }),
        animate('0.4s ease', style({ transform: 'translate(0, 0)' }))
      ]),
      transition(':leave', [
        animate('0.4s ease', style({ transform: 'translate(0, 140%)' }))
      ])
    ])
  ]
})
export class NotificationComponent implements OnInit, OnDestroy {
  public message: string;
  private messageSubscription: Subscription;

  constructor(private notificationService: NotificationService) {}

  ngOnInit() {
    this.messageSubscription = this.notificationService.message$.subscribe(
      (message: string) => (this.message = message)
    );
  }

  ngOnDestroy() {
    this.messageSubscription.unsubscribe();
  }

  public hide(): void {
    this.notificationService.hide();
  }
}
