import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  private _message = new BehaviorSubject<string>('');
  public message$ = this._message.asObservable();
  private timeOutId: number;
  private readonly timeOutTimer = 3000;
  private readonly animationDuration = 400;

  private get message(): string {
    return this._message.getValue();
  }

  public show(message: string): void {
    if (this.message) {
      this.hide();

      window.setTimeout(() => this._show(message), this.animationDuration);
    } else {
      this._show(message);
    }
  }

  private _show(message: string): void {
    this._message.next(message);

    this.timeOutId = window.setTimeout(() => this.hide(), this.timeOutTimer);
  }

  public hide(): void {
    if (this.timeOutId) {
      window.clearTimeout(this.timeOutId);
    }

    this._message.next('');
  }
}
