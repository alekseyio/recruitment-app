import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { RequestsService } from '../requests.service';
import { SidebarService } from '../../shared/services/sidebar.service';

@Component({
  selector: 'app-recruiter-home',
  templateUrl: './recruiter-home.component.html',
  providers: [RequestsService]
})
export class RecruiterHomeComponent implements OnInit {
  constructor(
    private router: Router,
    private requestsService: RequestsService,
    private sidebarService: SidebarService
  ) {}

  ngOnInit() {
    this._getCount();

    if (this.router.url === '/recruiter') {
      this.router.navigate(['recruiter', 'new-requests']);
    }
  }

  private _getCount() {
    this.requestsService
      .getNewRequestsCount()
      .subscribe((count: number) => this.sidebarService.setCount(count));

    setTimeout(() => this._getCount(), 300000); // Every 5 min
  }
}
