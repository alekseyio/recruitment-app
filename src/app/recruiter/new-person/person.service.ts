import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { IPersonBlank } from './Person.interface';
import { StorageService } from 'src/app/services/storage.service';

@Injectable()
export class PersonService {
  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {}

  public createPerson = (person: IPersonBlank) => {
    const url = '/person';
    const headers = new HttpHeaders({
      Authorization: this.storageService.get('token'),
    });

    return this.http.post(url, person, { headers });
  };
}
