import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

import { PersonService } from './person.service';
import { IPersonBlank, IPersonBlankErrors } from './Person.interface';

@Component({
  selector: 'app-new-person',
  templateUrl: './new-person.component.html',
  styleUrls: ['./new-person.component.scss'],
  providers: [PersonService],
})
export class RecruiterNewPersonComponent {
  public form: FormGroup = new FormGroup({
    lastName: new FormControl(''),
    firstName: new FormControl(''),
    middleName: new FormControl(''),
    age: new FormControl(''),
    citizenship: new FormControl(''),
    phone: new FormControl(''),
    comment: new FormControl(''),
  });
  public errors: IPersonBlankErrors = {};

  constructor(
    private personService: PersonService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  public onSubmit = (): void => {
    const person = this.form.value as IPersonBlank;

    this.personService
      .createPerson(person)
      .subscribe(this.handleReqSuccess, this.handleReqError);
  };

  private handleReqSuccess = () => {
    this.router.navigate(['..'], { relativeTo: this.route });
  };

  private handleReqError = (err: HttpErrorResponse) => {
    if (err.status === 400) {
      // Validation error
      this.errors = err.error.errors;
    } else {
      console.log(err);
    }
  };
}
