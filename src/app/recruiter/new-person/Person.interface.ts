export interface IPersonBlank {
  lastName: string;
  firstName: string;
  middleName: string;
  age: number;
  phone: string;
  citizenship: string;
  comment: string;
}

export interface IPersonBlankErrors {
  lastName?: string;
  firstName?: string;
  middleName?: string;
  age?: number;
  phone?: string;
  citizenship?: string;
  comment?: string;
}
