import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { Request } from '../request.interface';
import { StorageService } from '../services/storage.service';
import { Company } from '../director/new-request/add-company/company.interface';

export interface IRequestFilters {
  position: string;
  schedule: string;
}

@Injectable()
export class RequestsService {
  private _newRequests = new BehaviorSubject<Request[]>([]);
  public newRequests$ = this._newRequests.asObservable();

  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {}

  private get newRequests(): Request[] {
    return this._newRequests.getValue();
  }

  private set newRequests(requests: Request[]) {
    this._newRequests.next(requests);
  }

  public fetchNewRequests() {
    const url = '/requests/new';
    const headers = new HttpHeaders({
      Authorization: this.storageService.get('token'),
    });

    return this.http.get(url, { headers }).pipe(
      map((response: { requests: Request[] }) => response.requests),
      tap(requests => (this.newRequests = requests))
    );
  }

  public fetchNewRequestById(requestId: string) {
    const url = `/requests/new/${requestId}`;
    const headers = new HttpHeaders({
      Authorization: this.storageService.get('token'),
    });

    return this.http.get<Request>(url, { headers });
  }

  public deleteRequest(requestId: string) {
    const url = `/requests/${requestId}`;
    const headers = new HttpHeaders({
      Authorization: this.storageService.get('token'),
    });

    return this.http.delete(url, { headers });
  }

  public setRequestProcessing(requestId: string) {
    const url = `/requests/new/${requestId}`;
    const headers = new HttpHeaders({
      Authorization: this.storageService.get('token'),
    });

    return this.http.patch(url, null, { headers });
  }

  public fetchProcessingRequests(): Observable<Request[]> {
    const url = '/requests/processing';
    const headers = new HttpHeaders({
      Authorization: this.storageService.get('token'),
    });

    return this.http
      .get(url, { headers })
      .pipe(map((response: { requests: Request[] }) => response.requests));
  }

  public fetchProcessingRequestById(requestId: string): Observable<Request> {
    const url = `/requests/processing/${requestId}`;
    const headers = new HttpHeaders({
      Authorization: this.storageService.get('token'),
    });

    return this.http
      .get(url, { headers })
      .pipe(map((response: { request: Request }) => response.request));
  }

  public setRequestNew(requestId: string) {
    const url = `/requests/processing/${requestId}`;
    const headers = new HttpHeaders({
      Authorization: this.storageService.get('token'),
    });

    return this.http.patch(url, {}, { headers });
  }

  public getNewRequestsCount(): Observable<number> {
    const url = '/requests/new/count';
    const headers = new HttpHeaders({
      Authorization: this.storageService.get('token'),
    });

    return this.http.get<number>(url, { headers });
  }

  public sort(criteria: string, requests: Request[]): Request[] {
    switch (criteria) {
      case 'timeAsc': {
        return this.sortByTimeAsc(requests);
      }
      case 'timeDesc': {
        return this.sortByTimeDesc(requests);
      }
      case 'rateAsc': {
        return this.sortByRateAsc(requests);
      }
      case 'rateDesc': {
        return this.sortByRateDesc(requests);
      }
      default: {
        return requests;
      }
    }
  }

  private sortByTimeAsc = (requests: Request[]): Request[] =>
    requests.sort(
      (reqA: Request, reqB: Request) =>
        new Date(reqA.createdAt).getTime() - new Date(reqB.createdAt).getTime()
    );

  private sortByTimeDesc = (requests: Request[]): Request[] =>
    requests.sort(
      (reqA: Request, reqB: Request) =>
        new Date(reqB.createdAt).getTime() - new Date(reqA.createdAt).getTime()
    );

  private sortByRateAsc = (requests: Request[]): Request[] =>
    requests.sort((reqA: Request, reqB: Request) => reqA.rate - reqB.rate);

  private sortByRateDesc = (requests: Request[]): Request[] =>
    requests.sort((reqA: Request, reqB: Request) => reqB.rate - reqA.rate);

  public filterRequests = <K extends keyof IRequestFilters>(
    requests: Request[],
    filters: IRequestFilters
  ): Request[] => {
    let result: Request[] = [];
    let firstRun = true;

    for (let key in filters) {
      if (firstRun) {
        result = this.filterRequestsByField(requests, <K>key, filters[key]);
        firstRun = false;
      } else {
        result = this.filterRequestsByField(result, <K>key, filters[key]);
      }
    }

    return result;
  };

  public filterRequestsByField = <K extends keyof IRequestFilters>(
    requests: Request[],
    field: K,
    value: string
  ): Request[] =>
    requests.filter(
      (request: Request) =>
        request[field]
          .toString()
          .toLowerCase()
          .indexOf(value.toLowerCase()) !== -1
    );

  public filterRequestsByCompanyStringField = <K extends keyof Company>(
    requests: Request[],
    key: string,
    val: string
  ): Request[] =>
    requests.filter(
      request =>
        request.user.company[key]
          .toString()
          .toLowerCase()
          .indexOf(val.toLowerCase()) !== -1
    );

  public filterRequestsBySubwayLine = (
    requests: Request[],
    val: number
  ): Request[] =>
    requests.filter(request => request.user.company.subwayLine === val);
}
