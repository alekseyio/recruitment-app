import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '../shared/shared.module';
import { RecruiterRoutingModule } from './recruiter-routing.module';
import { RecruiterHomeComponent } from './recruiter-home/recruiter-home.component';
import { RecruiterNewRequestsComponent } from './requests/new-requests/new-requests.component';
import { RecruiterAllRequestsComponent } from './requests/all-requests/all-requests.component';
import { RecruiterDatabaseComponent } from './database/database.component';
import { RecruiterProfileComponent } from './profile/profile.component';
import { RecruiterRequestsListComponent } from './requests/requests-list/requests-list.component';
import { RecruiterRequestsItemComponent } from './requests/requests-list/requests-item/requests-item.component';
import { RecruiterRequestsTableComponent } from './requests/requests-table/requests-table.component';
import { RecruiterNewRequestComponent } from './requests/new-request/new-request.component';
import { RecruiterPeopleTableComponent } from './database/people-table/people-table.component';
import { RecruiterPeopleListComponent } from './database/people-list/people-list.component';
import { RecruiterPersonItemComponent } from './database/people-list/person-item/person-item.component';
import { RecruiterPersonDetailComponent } from './database/person-detail/person-detail.component';
import { RecruiterRequestComponent } from './requests/request/request.component';
import { RecruiterRequestDetailComponent } from './requests/request/request-detail/request-detail.component';
import { RecruiterAddCandidateComponent } from './requests/request/add-candidate/add-candidate.component';
import { RecruiterAddPersonComponent } from './requests/request/add-candidate/add-person/add-person.component';
import { RecruiterAddExistentComponent } from './requests/request/add-candidate/add-existent/add-existent.component';
import { RecruiterInterviewsListComponent } from './requests/request/interviews-list/interviews-list.component';
import { RecruiterInterviewComponent } from './requests/request/interviews-list/interview/interview.component';
import { RecruiterNewPersonComponent } from './new-person/new-person.component';
import {NgbDatepickerModule, NgbTimepickerModule} from '@ng-bootstrap/ng-bootstrap';
import {AngularMyDatePickerModule} from 'angular-mydatepicker';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RecruiterRoutingModule,
    SharedModule,
    NgbDatepickerModule,
    NgbTimepickerModule,
    AngularMyDatePickerModule
  ],
  declarations: [
    RecruiterHomeComponent,
    RecruiterNewRequestsComponent,
    RecruiterAllRequestsComponent,
    RecruiterRequestComponent,
    RecruiterRequestDetailComponent,
    RecruiterAddCandidateComponent,
    RecruiterAddPersonComponent,
    RecruiterAddExistentComponent,
    RecruiterInterviewsListComponent,
    RecruiterInterviewComponent,
    RecruiterRequestsListComponent,
    RecruiterRequestsItemComponent,
    RecruiterRequestsTableComponent,
    RecruiterNewRequestComponent,
    RecruiterDatabaseComponent,
    RecruiterPeopleTableComponent,
    RecruiterPeopleListComponent,
    RecruiterPersonItemComponent,
    RecruiterPersonDetailComponent,
    RecruiterProfileComponent,
    RecruiterNewPersonComponent,
  ],
})
export class RecruiterModule {}
