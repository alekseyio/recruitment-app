import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

import { IPerson } from '../../../interfaces/Person.interface';

@Component({
  selector: 'app-recruiter-people-table',
  templateUrl: './people-table.component.html',
  styleUrls: ['./people-table.component.scss'],
})
export class RecruiterPeopleTableComponent {
  @Input() people: IPerson[];

  constructor(private router: Router) {}

  public handleRowClick(person: IPerson): void {
    this.router.navigate(['recruiter', 'person', person.id]);
  }
}
