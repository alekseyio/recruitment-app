import { Component, OnInit, OnDestroy } from '@angular/core';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { PersonService } from './person.service';
import { Person } from './person.interface';

@Component({
  selector: 'app-recruiter-database',
  templateUrl: './database.component.html',
  styleUrls: ['./database.component.scss'],
  providers: [PersonService],
})
export class RecruiterDatabaseComponent implements OnInit, OnDestroy {
  public people: Person[];
  public isFetching: boolean;
  public isTableView = true;
  public toggleShown = true;
  private breakpointSubscription: Subscription;

  constructor(
    private personService: PersonService,
    private breakpointObserver: BreakpointObserver,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.isFetching = true;

    this.personService.fetchPeople().subscribe(people => {
      this.people = people;
      this.isFetching = false;
    });

    this.breakpointSubscription = this.breakpointObserver
      .observe('(max-width: 87.5rem)')
      .subscribe(this.handleBreakpoint.bind(this));
  }

  ngOnDestroy() {
    this.breakpointSubscription.unsubscribe();
  }

  public handleCheckboxChange(checked: boolean): void {
    this.isTableView = checked;
  }

  private handleBreakpoint(result: BreakpointState): void {
    if (result.matches) {
      this.isTableView = false;
      this.toggleShown = false;
    } else {
      this.isTableView = true;
      this.toggleShown = true;
    }
  }

  public onAddPerson = (): void => {
    this.router.navigate(['add'], { relativeTo: this.route });
  };
}
