import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

import { IPerson } from '../../../../interfaces/Person.interface';

@Component({
  selector: 'app-recruiter-person-item',
  templateUrl: './person-item.component.html',
  styleUrls: ['./person-item.component.scss'],
})
export class RecruiterPersonItemComponent {
  @Input() person: IPerson;

  constructor(private router: Router) {}

  public handleDetailsButtonClick(): void {
    this.router.navigate(['recruiter', 'person', this.person.id]);
  }
}
