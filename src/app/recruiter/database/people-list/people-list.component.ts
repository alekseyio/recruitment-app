import { Component, Input } from '@angular/core';

import { IPerson } from '../../../interfaces/Person.interface';

@Component({
  selector: 'app-recruiter-people-list',
  templateUrl: './people-list.component.html',
  styleUrls: ['./people-list.component.scss'],
})
export class RecruiterPeopleListComponent {
  @Input() people: IPerson[];
}
