import { Company } from '../../director/new-request/add-company/company.interface';

export interface Person {
  _id: string;
  id: string;
  firstName: string;
  lastName: string;
  middleName: string;
  age: number;
  nationality: string;
  phone: string;
  employed: boolean;
  comment: string;
  createdAt: Date;
  job?: {
    company: Company;
    position: string;
    schedule: string;
    rate: number;
  };
}
