import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { switchMap } from 'rxjs/operators';

import { PersonService } from '../person.service';
import { NotificationService } from '../../../notifications/notification.service';
import { IPerson } from '../../../interfaces/Person.interface';

@Component({
  selector: 'app-recruiter-person-detail',
  templateUrl: './person-detail.component.html',
  styleUrls: ['./person-detail.component.scss'],
  providers: [PersonService],
})
export class RecruiterPersonDetailComponent implements OnInit {
  public person: IPerson;
  public isFetching: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private personService: PersonService,
    private notificationService: NotificationService
  ) {}

  ngOnInit() {
    this.isFetching = true;

    this.route.paramMap
      .pipe(
        switchMap(params => {
          const id = params.get('id');

          return this.personService.fetchPerson(id);
        })
      )
      .subscribe(
        this.handleRequest.bind(this),
        this.handleRequestError.bind(this)
      );
  }

  private handleRequest(person: IPerson): void {
    this.person = person;
    this.isFetching = false;
  }

  private handleRequestError(err: HttpErrorResponse): void {
    this.isFetching = false;

    if (err.status === 404) {
      this.router.navigate(['recruiter', 'database']);
      this.notificationService.show(
        'Соискателя с таким идентификатором нет в системе'
      );
    } else {
      this.router.navigateByUrl('/server-offline');
      this.notificationService.show('Произошла ошибка на стороне сервера');
    }
  }
}
