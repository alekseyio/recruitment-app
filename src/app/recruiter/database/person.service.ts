import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { StorageService } from '../../services/storage.service';
import { Person } from './person.interface';

@Injectable()
export class PersonService {
  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {}

  public fetchPeople() {
    const url = '/person';
    const headers = new HttpHeaders({
      Authorization: this.storageService.get('token')
    });

    return this.http
      .get(url, { headers })
      .pipe(map((response: { people: Person[] }) => response.people));
  }

  public fetchPerson(personId: string) {
    const url = `/person/${personId}`;
    const headers = new HttpHeaders({
      Authorization: this.storageService.get('token')
    });

    return this.http
      .get(url, { headers })
      .pipe(map((response: { person: Person }) => response.person));
  }
}
