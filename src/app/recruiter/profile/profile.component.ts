import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';

import { AuthService } from '../../auth/services/auth.service';
import { User } from '../../auth/interfaces/User.interface';
import { StorageService } from '../../services/storage.service';

@Component({
  selector: 'app-recruiter-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class RecruiterProfileComponent implements OnInit {
  public user: User;

  constructor(
    private authService: AuthService,
    private storageService: StorageService,
    private router: Router
  ) {}

  ngOnInit() {
    this.authService.user$.pipe(take(1)).subscribe(user => (this.user = user));
  }

  public get userNameObj(): {
    firstName: string;
    lastName: string;
    middleName: string;
  } {
    return {
      firstName: this.user.firstName,
      lastName: this.user.lastName,
      middleName: this.user.middleName
    };
  }

  public handleLogoutButtonClick(): void {
    this.storageService.clear('token');
    this.authService.setUser(null);
    this.router.navigate(['auth', 'login']);
  }
}
