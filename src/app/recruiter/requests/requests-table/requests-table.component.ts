import { Component, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Request } from '../../../request.interface';

@Component({
  selector: 'app-recruiter-requests-table',
  templateUrl: './requests-table.component.html',
  styleUrls: ['./requests-table.component.scss']
})
export class RecruiterRequestsTableComponent {
  @Input() requests: Request[];

  constructor(private route: ActivatedRoute, private router: Router) {}

  public handleRowClick(request: Request) {
    this.router.navigate([request.id], { relativeTo: this.route });
  }
}
