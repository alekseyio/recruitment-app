import { Component, OnInit, OnDestroy } from '@angular/core';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { Subscription } from 'rxjs';

import { RequestsService } from '../../requests.service';
import { Request } from '../../../request.interface';

@Component({
  selector: 'app-recruiter-all-requests',
  templateUrl: './all-requests.component.html',
  styleUrls: ['./all-requests.component.scss'],
})
export class RecruiterAllRequestsComponent implements OnInit, OnDestroy {
  public requests: Request[];
  public requestsToRender: Request[];
  public isFetching: boolean;
  public isTableView = true;
  public toggleShown = true;
  private breakpointSubscription: Subscription;

  public sortCriteria: string = '';
  public positionFilter: string = '';
  public scheduleFilter: string = '';
  public addressFilter = '';
  public subwayFilter = '';
  public subwayLineFilter = '';

  constructor(
    private requestsService: RequestsService,
    private breakpointObserver: BreakpointObserver
  ) {}

  ngOnInit() {
    this.isFetching = true;

    this.requestsService.fetchProcessingRequests().subscribe(requests => {
      this.requests = requests;
      this.requestsToRender = requests;
      this.isFetching = false;
    });

    this.breakpointSubscription = this.breakpointObserver
      .observe('(max-width: 87.5em)')
      .subscribe(this.handleBreakpoint.bind(this));
  }

  ngOnDestroy() {
    this.breakpointSubscription.unsubscribe();
  }

  public handleCheckboxChange(checked: boolean): void {
    this.isTableView = checked;
  }

  private handleBreakpoint(result: BreakpointState): void {
    if (result.matches) {
      this.isTableView = false;
      this.toggleShown = false;
    } else {
      this.isTableView = true;
      this.toggleShown = true;
    }
  }

  public onSortChange = (e: Event): void => {
    this.requestsToRender = this.sortRequests(
      this.requestsToRender.slice(),
      this.sortCriteria
    );
  };

  private sortRequests = (requests: Request[], criteria: string): Request[] => {
    return this.requestsService.sort(criteria, requests);
  };

  public onFilterFieldChange = (requests?: Request[]): void => {
    let sorted: Request[] = this.requests.slice();

    if (typeof requests !== 'undefined') {
      sorted = requests;
    }

    if (this.sortCriteria !== '') {
      sorted = this.sortRequests(sorted, this.sortCriteria);
    }

    const filters = {
      position: this.positionFilter,
      schedule: this.scheduleFilter,
    };

    this.requestsToRender = this.requestsService.filterRequests(
      sorted,
      filters
    );
  };

  public filterRequestsByCompanyField(key: string): void {
    if (key === 'subway') {
      const requests = this.requestsService.filterRequestsByCompanyStringField(
        this.requests.slice(),
        'subway',
        this.subwayFilter
      );
      this.onFilterFieldChange(requests);
    } else if (key === 'address') {
      const requests = this.requestsService.filterRequestsByCompanyStringField(
        this.requests.slice(),
        'address',
        this.addressFilter
      );
      this.onFilterFieldChange(requests);
    } else if (key === 'subwayLine') {
      if (this.subwayLineFilter === '') {
        this.onFilterFieldChange();
      } else {
        const requests = this.requestsService.filterRequestsBySubwayLine(
          this.requests.slice(),
          +this.subwayLineFilter
        );
        this.onFilterFieldChange(requests);
      }
    }
  }

  public onClearFiltersClick = () => {
    this.clearFilters();

    this.requestsToRender =
      this.sortCriteria !== ''
        ? this.requestsService.sort(this.sortCriteria, this.requests.slice())
        : this.requests.slice();
  };

  private clearFilters = () => {
    this.positionFilter = '';
    this.scheduleFilter = '';
    this.subwayFilter = '';
    this.addressFilter = '';
    this.subwayLineFilter = '';
  };
}
