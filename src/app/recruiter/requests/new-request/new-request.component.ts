import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { switchMap } from 'rxjs/operators';

import { RequestsService } from '../../requests.service';
import { Request } from '../../../request.interface';
import { NotificationService } from '../../../notifications/notification.service';

enum Operation {
  ChangeStatus,
  Delete
}

@Component({
  selector: 'app-recruiter-new-request',
  templateUrl: './new-request.component.html',
  styleUrls: ['./new-request.component.scss']
})
export class RecruiterNewRequestComponent implements OnInit {
  public request: Request;
  public isFetching = false;
  public modalShown = false;
  private lastOperation: Operation;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private requestsService: RequestsService,
    private notificationService: NotificationService
  ) {}

  ngOnInit() {
    this.isFetching = true;

    this.route.paramMap
      .pipe(
        switchMap(params =>
          this.requestsService.fetchNewRequestById(params.get('id'))
        )
      )
      .subscribe(
        this.handleRequest.bind(this),
        this.handleRequestError.bind(this)
      );
  }

  private handleRequest(response: { request: Request }): void {
    this.request = response.request;
    this.isFetching = false;
  }

  private handleRequestError(err: HttpErrorResponse): void {
    this.isFetching = false;

    if (err.status === 404) {
      this.notificationService.show(
        'Заявки с таким идентификатором нет в системе'
      );
      this.router.navigate(['..'], { relativeTo: this.route });
    } else {
      this.notificationService.show('Произошла ошибка на стороне сервера');
      this.router.navigate(['server-offline']);
    }
  }

  public handleModalConfirm(): void {
    if (this.lastOperation === Operation.ChangeStatus) {
      this.changeRequestStatus();
    } else if (this.lastOperation === Operation.Delete) {
      this.deleteRequest();
    }

    this.modalShown = false;
    this.lastOperation = null;
  }

  public handleModalClose(): void {
    this.modalShown = false;
  }

  public handleToggleButtonClick(): void {
    this.modalShown = true;
    this.lastOperation = Operation.ChangeStatus;
  }

  public handleDeleteButtonClick(): void {
    this.modalShown = true;
    this.lastOperation = Operation.Delete;
  }

  private deleteRequest(): void {
    this.requestsService.deleteRequest(this.request.id).subscribe(() => {
      this.router.navigate(['..']);
      this.notificationService.show('Заявка успешно удалена');
    });
  }

  private changeRequestStatus(): void {
    this.requestsService.setRequestProcessing(this.request.id).subscribe(() => {
      this.router.navigate(['..']);
      this.notificationService.show('Заявка была перемещена в общий список');
    });
  }
}
