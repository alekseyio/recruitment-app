import { Component, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Request } from '../../../../request.interface';

@Component({
  selector: 'app-recruiter-requests-item',
  templateUrl: './requests-item.component.html',
  styleUrls: ['./requests-item.component.scss']
})
export class RecruiterRequestsItemComponent {
  @Input() request: Request;

  constructor(private router: Router, private route: ActivatedRoute) {}

  public handleDetailsButtonClick() {
    this.router.navigate([this.request.id], { relativeTo: this.route });
  }
}
