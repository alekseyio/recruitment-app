import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { StorageService } from '../../../../../services/storage.service';

@Injectable()
export class AddPersonService {
  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {}

  public createInterview(values) {
    const url = `/interviews/person`;
    const headers = new HttpHeaders({
      Authorization: this.storageService.get('token'),
    });

    return this.http.post(url, values, { headers });
  }
}
