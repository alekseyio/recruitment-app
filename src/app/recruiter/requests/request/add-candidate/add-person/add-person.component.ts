import {
  Component,
  Output,
  EventEmitter,
  Input,
  ViewChild,
} from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

import { AddPersonService } from './add-person.service';
import { Request } from '../../../../../request.interface';
import { NotificationService } from '../../../../../notifications/notification.service';
import {
  AngularMyDatePickerDirective,
  IAngularMyDpOptions,
  IMyDateModel,
  IMyInputFieldChanged,
} from 'angular-mydatepicker';

interface FormErrors {
  [key: string]: string;
}

@Component({
  selector: 'app-recruiter-add-person',
  templateUrl: './add-person.component.html',
  styleUrls: ['./add-person.component.scss'],
  providers: [AddPersonService],
})
export class RecruiterAddPersonComponent {
  @Input() request: Request;
  @Output() back: EventEmitter<void> = new EventEmitter<void>();
  public form: FormGroup;
  public errors: FormErrors;
  public btnDisabled: boolean = false;

  @ViewChild('dp', { static: false }) mydp: AngularMyDatePickerDirective;
  public datePickerOptions: IAngularMyDpOptions = {
    disableUntil: {
      year: new Date().getFullYear(),
      month: new Date().getMonth() + 1,
      day: new Date().getDate() - 1,
    },
    dateFormat: 'dd.mm.yyyy',
    alignSelectorRight: true,
    appendSelectorToBody: true,
    divHostElement: { enabled: true, placeholder: 'dd.mm.yyyy' },
  };

  constructor(
    private router: Router,
    private addPersonService: AddPersonService,
    private notificationService: NotificationService,
    private route: ActivatedRoute
  ) {
    this.initForm();
    this.initErrors();
  }

  private initForm = (): void => {
    this.form = new FormGroup({
      lastName: new FormControl(''),
      firstName: new FormControl(''),
      middleName: new FormControl(''),
      age: new FormControl(null),
      citizenship: new FormControl(''),
      phone: new FormControl(''),
      comment: new FormControl(''),
      resource: new FormControl(''),
      interviewDate: new FormControl(),
      interviewTime: new FormControl(),
    });
  };

  private initErrors = (): void => {
    this.errors = {};
  };

  public handleBackClick = (): void => {
    this.back.emit();
  };

  public handleSubmit = (): void => {
    if (
      this.form.value.interviewDate === null ||
      this.form.value.interviewTime === null
    ) {
      this.errors = {
        date: 'Дата должна быть заполнена',
      };
    } else if (!this.mydp.isDateValid()) {
      this.errors = {
        date: 'Дата имеет неправильный формат',
      };
    } else {
      this.initErrors();
      this.btnDisabled = true;
      this.errors = {};
      const date =
        this.form.value.interviewDate &&
        this.form.value.interviewDate.singleDate.date;
      const time = this.form.value.interviewTime;

      const values = {
        ...this.form.value,
        date: new Date(
          date.year,
          date.month - 1,
          date.day,
          time.hour,
          time.minute
        ),
        requestId: this.request.id,
      };

      console.log(values);
      this.addPersonService
        .createInterview(values)
        .subscribe(this.handleRequestSuccess, this.handleRequestError);
    }
  };

  private handleRequestSuccess = (request: Request): void => {
    this.router.navigate(['..'], { relativeTo: this.route });
    this.notificationService.show('Интервью было успешно назначено');
  };

  private handleRequestError = (err: HttpErrorResponse): void => {
    this.btnDisabled = false;

    if (err.status === 400) {
      this.errors = err.error.errors;
    }
  };
}
