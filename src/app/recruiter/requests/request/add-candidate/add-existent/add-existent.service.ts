import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { StorageService } from '../../../../../services/storage.service';
import { IPerson } from '../../../../../interfaces/Person.interface';

@Injectable()
export class AddExistentService {
  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {}

  public getAvailableCandidates(requestId: string) {
    const url = `/interviews/request/${requestId}/candidates`;
    const headers = new HttpHeaders({
      Authorization: this.storageService.get('token'),
    });

    return this.http.get<IPerson[]>(url, { headers });
  }

  public createInterview(values: {
    requestId: string;
    personId: string;
    date: Date;
  }) {
    const url = `/interviews`;
    const headers = new HttpHeaders({
      Authorization: this.storageService.get('token'),
    });

    return this.http.post(url, values, { headers });
  }
}
