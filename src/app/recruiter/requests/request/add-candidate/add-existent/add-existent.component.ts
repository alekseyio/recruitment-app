import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

import { Request } from '../../../../../request.interface';
import { AddExistentService } from './add-existent.service';
import { Person } from '../../../../database/person.interface';
import { NotificationService } from 'src/app/notifications/notification.service';
import { IPerson } from 'src/app/interfaces/Person.interface';
import {
  AngularMyDatePickerDirective,
  IAngularMyDpOptions,
  IMyDateModel,
  IMyInputFieldChanged,
} from 'angular-mydatepicker';

interface FormErrors {
  personId?: string;
  date?: string;
}

@Component({
  selector: 'app-recruiter-add-existent',
  templateUrl: './add-existent.component.html',
  styleUrls: ['./add-existent.component.scss'],
  providers: [AddExistentService],
})
export class RecruiterAddExistentComponent implements OnInit {
  @Input() request: Request;
  @Output() back: EventEmitter<void> = new EventEmitter<void>();
  public candidates: IPerson[];
  public isFetching: boolean;
  public form: FormGroup;
  public errors: FormErrors;
  public btnDisabled = false;

  @ViewChild('dp', { static: false }) mydp: AngularMyDatePickerDirective;
  public datePickerOptions: IAngularMyDpOptions = {
    disableUntil: {
      year: new Date().getFullYear(),
      month: new Date().getMonth() + 1,
      day: new Date().getDate() - 1,
    },
    dateFormat: 'dd.mm.yyyy',
    alignSelectorRight: true,
    appendSelectorToBody: true,
    divHostElement: { enabled: true, placeholder: 'dd.mm.yyyy' },
  };

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private addExistentService: AddExistentService,
    private notificationService: NotificationService
  ) {}

  ngOnInit() {
    this.initForm();
    this.initErrors();
    this.isFetching = true;

    this.addExistentService
      .getAvailableCandidates(this.request.id)
      .subscribe(candidates => {
        if (!candidates.length) {
          this.router.navigate(['..'], { relativeTo: this.route });
          this.notificationService.show(
            'Для данного собеседования нет свободных кандидатов'
          );
        }

        this.candidates = candidates;
        this.isFetching = false;
      });
  }

  private initForm = (): void => {
    this.form = new FormGroup({
      personId: new FormControl(''),
      // interviewDate: new FormControl(new Date()),
      interviewDate: new FormControl(),
      interviewTime: new FormControl(),
    });
  };

  private initErrors = (): void => {
    this.errors = {};
  };

  onDateChanged(event: IMyDateModel): void {
    // console.log(event);
  }

  onInputFieldChanged(event: IMyInputFieldChanged): void {
    // console.log(event);
  }

  // call the clearDate() function of the directive
  clearDate(): void {
    this.mydp.clearDate();
  }

  // call the isDateValid() function of the directive
  checkDateValidity(): void {
    const valid: boolean = this.mydp.isDateValid();
    console.log('Valid date in the input box: ', valid);
  }

  public handleBackClick = (): void => {
    this.back.emit();
  };

  public handleSubmit = (): void => {
    // console.log(this.form.value)
    if (
      this.form.value.interviewDate === null ||
      this.form.value.interviewTime === null
    ) {
      this.errors = {
        date: 'Дата должна быть заполнена',
      };
    } else if (!this.mydp.isDateValid()) {
      this.errors = {
        date: 'Дата имеет неправильный формат',
      };
    } else {
      this.btnDisabled = true;
      this.errors = {};
      const date =
        this.form.value.interviewDate &&
        this.form.value.interviewDate.singleDate.date;
      const time = this.form.value.interviewTime;
      const values = {
        requestId: this.request.id,
        personId: this.form.value.personId,
        date: new Date(
          date.year,
          date.month - 1,
          date.day,
          time.hour,
          time.minute
        ),
      };

      this.addExistentService
        .createInterview(values)
        .subscribe(this.onCreateInterview, this.onCreateInterviewErr);
    }
  };

  private onCreateInterview = () => {
    this.router.navigate(['..'], { relativeTo: this.route });
    this.notificationService.show('Интервью успешно назначено');
  };

  private onCreateInterviewErr = (err: HttpErrorResponse) => {
    if (err.status === 400) {
      this.errors = err.error.errors;
      this.btnDisabled = false;
    } else {
      console.log(err);
    }
  };
}
