export interface FormValues {
  personId: string;
  interviewDate: Date;
}
