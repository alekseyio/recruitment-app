import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { switchMap } from 'rxjs/operators';

import { RequestsService } from '../../../requests.service';
import { Request } from '../../../../request.interface';

enum State {
  Choose,
  AddExistent,
  AddNew
}

@Component({
  selector: 'app-recruiter-add-candidate',
  templateUrl: './add-candidate.component.html',
  styleUrls: ['./add-candidate.component.scss']
})
export class RecruiterAddCandidateComponent implements OnInit {
  public State = State;
  public currentState: State = State.Choose;
  public request: Request;
  public isFetching: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private requestsService: RequestsService
  ) {}

  ngOnInit() {
    this.isFetching = true;

    this.route.paramMap
      .pipe(
        switchMap(params => {
          const id = params.get('id');

          return this.requestsService.fetchProcessingRequestById(id);
        })
      )
      .subscribe(this.handleRequestSuccess, this.handleRequestError);
  }

  public setCurrentState = (newState: State): void => {
    this.currentState = newState;
  };

  private handleRequestSuccess = (request: Request): void => {
    this.request = request;
    this.isFetching = false;
  };

  private handleRequestError = (err: HttpErrorResponse): void => {
    this.isFetching = false;

    if (err.status === 404) {
      this.router.navigate(['recruiter', 'all-requests']);
    } else if (err.status === 500) {
      this.router.navigate(['server-offline']);
    }
  };
}
