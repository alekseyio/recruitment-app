import { Component, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Request } from '../../../../request.interface';
import { RequestsService } from '../../../requests.service';
import { NotificationService } from '../../../../notifications/notification.service';

@Component({
  selector: 'app-recruiter-request-detail',
  templateUrl: './request-detail.component.html',
  styleUrls: ['./request-detail.component.scss'],
})
export class RecruiterRequestDetailComponent {
  public modalShown = false;

  constructor(
    private requestsService: RequestsService,
    private router: Router,
    private route: ActivatedRoute,
    private notificationService: NotificationService
  ) {}

  @Input() request: Request;

  public handleAddCandidateClick(): void {
    this.router.navigate(['add-candidate'], { relativeTo: this.route });
  }
}
