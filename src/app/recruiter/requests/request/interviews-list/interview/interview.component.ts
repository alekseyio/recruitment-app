import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit,
  ViewChild,
} from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

import { InterviewsService } from '../../interviews.service';
import { Router } from '@angular/router';
import { NotificationService } from 'src/app/notifications/notification.service';
import { IInterview } from 'src/app/interfaces/Interview.interface';
import { IRequest } from 'src/app/interfaces/Request.interface';
import {
  AngularMyDatePickerDirective,
  IAngularMyDpOptions,
} from 'angular-mydatepicker';

// interface FormErrors {
//   personId?: string;
//   date?: string;
// }

@Component({
  selector: 'app-recruiter-interview',
  templateUrl: './interview.component.html',
  styleUrls: ['./interview.component.scss'],
})
export class RecruiterInterviewComponent implements OnInit {
  @Input() interview: IInterview;
  @Input() request: IRequest;
  @Output() interviewDeleted: EventEmitter<string> = new EventEmitter<string>();
  public changingDate = false;
  public newInterviewDate: any;
  public newInterviewTime: any;
  // public errors: FormErrors;

  @ViewChild('dp', { static: false }) mydp: AngularMyDatePickerDirective;
  public datePickerOptions: IAngularMyDpOptions = {
    disableUntil: {
      year: new Date().getFullYear(),
      month: new Date().getMonth() + 1,
      day: new Date().getDate() - 1,
    },
    dateRange: false,
    dateFormat: 'dd.mm.yyyy',
    alignSelectorRight: true,
    appendSelectorToBody: true,
    divHostElement: { enabled: true, placeholder: 'dd.mm.yyyy' },
  };

  constructor(
    private router: Router,
    private notificationService: NotificationService,
    private interviewsService: InterviewsService
  ) {}

  ngOnInit() {
    // this.initErrors();
    // this.newInterviewDate = new Date(this.interview.date)
    //   .toISOString()
    //   .slice(0, 16);
    const date = new Date(this.interview.date);
    this.newInterviewDate = {
      isRange: false,
      singleDate: {
        date: {
          year: date.getFullYear(),
          month: date.getMonth() + 1,
          day: date.getDate(),
        },
      },
    };
    this.newInterviewTime = {
      hour: date.getHours(),
      minute: date.getMinutes(),
    };
  }

  // private initErrors = (): void => {
  //   this.errors = {};
  // }

  public getResult(): string {
    const { result } = this.interview;

    if (!result) {
      return 'не известно';
    }

    switch (result) {
      case 'success': {
        return 'принят';
      }
      case 'fail': {
        return 'не принят';
      }
    }
  }

  public canChangeInterviewDate() {
    return (
      new Date(this.interview.date).getTime() > Date.now() &&
      !this.interview.result
    );
  }

  public onChangeDateClick = () => (this.changingDate = true);

  public onApproveNewDate = () => {
    console.log(this.newInterviewDate);
    // this.errors = {};
    const date = this.newInterviewDate && this.newInterviewDate.singleDate.date;
    const time = this.newInterviewTime;
    const newDate = new Date(
      date.year,
      date.month - 1,
      date.day,
      time.hour,
      time.minute
    );
    this.interviewsService
      .changeInterviewDate(this.interview.id, newDate)
      .subscribe(this.onChangeDate, this.onChangeDateError);
  };

  private onChangeDate = () => {
    const date = this.newInterviewDate && this.newInterviewDate.singleDate.date;
    const time = this.newInterviewTime;
    this.interview.date = new Date(
      date.year,
      date.month - 1,
      date.day,
      time.hour,
      time.minute
    ).toISOString();
    this.changingDate = false;
    this.notificationService.show('Время собеседования успешно изменено');
  };

  private onChangeDateError = (err: HttpErrorResponse) => {
    this.changingDate = false;
    this.notificationService.show(
      'Произошла ошибка во время изменения времени собеседования'
    );
  };

  public handleAttachClick(): void {
    const requestId = this.request.id;
    const interviewId = this.interview.id;
    const personId = this.interview.person._id;

    this.interviewsService
      .createEmployee({
        requestId,
        interviewId,
        personId,
      })
      .subscribe(this.handleAttachSuccess);
  }

  private handleAttachSuccess = (): void => {
    this.notificationService.show(
      'Кандидат был успешно закреплен за должностью'
    );
    this.router.navigate(['recruiter', 'all-requests']);
  };

  public handleDeleteInterviewClick(): void {
    this.interviewsService
      .setInterviewInactive(this.interview.id)
      .subscribe(() => {
        this.notificationService.show('Собеседование было закрыто');
        this.interviewDeleted.emit(this.interview.id);
      });
  }

  public onCancelInterview() {
    this.interviewsService
      .setInterviewInactive(this.interview.id)
      .subscribe(() => {
        this.notificationService.show('Собеседование было отменено');
        this.interviewDeleted.emit(this.interview.id);
      });
  }
}
