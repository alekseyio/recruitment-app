import { Component, Input } from '@angular/core';

import { Request } from 'src/app/request.interface';
import { IInterview } from '../../../../interfaces/Interview.interface';

@Component({
  selector: 'app-recruiter-interviews-list',
  templateUrl: './interviews-list.component.html',
  styleUrls: ['./interviews-list.component.scss'],
})
export class RecruiterInterviewsListComponent {
  @Input() interviews: IInterview[];
  @Input() request: Request;

  public onDeleteInterview(interviewId: string): void {
    this.interviews = this.interviews.filter(
      interview => interview.id !== interviewId
    );
  }
}
