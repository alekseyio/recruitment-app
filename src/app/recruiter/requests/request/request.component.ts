import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap } from 'rxjs/operators';

import { RequestsService } from '../../requests.service';
import { NotificationService } from '../../../notifications/notification.service';
import { InterviewsService } from './interviews.service';
import { IRequest } from '../../../interfaces/Request.interface';
import { IInterview } from 'src/app/interfaces/Interview.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-recruiter-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.scss'],
  providers: [InterviewsService],
})
export class RecruiterRequestComponent implements OnInit, OnDestroy {
  public request: IRequest;
  public interviews: IInterview[];
  public isFetching: boolean;
  public interviewDeletedSubscription: Subscription;

  constructor(
    private requestsService: RequestsService,
    private route: ActivatedRoute,
    private router: Router,
    private notificationService: NotificationService,
    private interviewsService: InterviewsService
  ) {}

  ngOnInit() {
    this.isFetching = true;
    this.interviewDeletedSubscription = this.interviewsService.interviewDeleted$.subscribe(
      interviewId =>
        (this.interviews = this.interviews.filter(i => i.id !== interviewId))
    );

    this.route.paramMap
      .pipe(
        switchMap(params => {
          const requestId = params.get('id');

          return this.requestsService.fetchProcessingRequestById(requestId);
        })
      )
      .subscribe(this.onFetchRequest, this.onFetchRequestError);
  }

  private onFetchRequest = request => {
    this.request = request as IRequest;
    this.interviewsService
      .getInterviews(request.id)
      .subscribe(this.onFetchInterviews, this.onFetchInterviewsError);
  };

  private onFetchRequestError = (err: HttpErrorResponse) => {
    this.isFetching = false;

    if (err.status === 404) {
      this.router.navigate(['recruiter', 'all-requests']);
      this.notificationService.show(
        'Заявки с таким идентификатором нет в системе'
      );
    }
  };

  private onFetchInterviews = (interviews: IInterview[]) => {
    this.interviews = interviews;
    this.isFetching = false;
  };

  private onFetchInterviewsError = (err: HttpErrorResponse) => {
    this.isFetching = false;
    console.log(err);
  };

  ngOnDestroy() {
    this.interviewDeletedSubscription.unsubscribe();
  }
}
