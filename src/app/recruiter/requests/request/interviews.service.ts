import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { StorageService } from '../../../services/storage.service';
import { Subject, Observable } from 'rxjs';

export interface INewEmployeeValues {
  requestId: string;
  interviewId: string;
  personId: string;
}

@Injectable()
export class InterviewsService {
  public interviewDeleted: Subject<string> = new Subject<string>();
  public interviewDeleted$: Observable<
    string
  > = this.interviewDeleted.asObservable();

  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {}

  public setInterviewInactive(interviewId: string) {
    const url = `/interviews/${interviewId}/inactive`;
    const headers = new HttpHeaders({
      Authorization: this.storageService.get('token'),
    });

    return this.http.patch(url, null, { headers });
  }

  public createEmployee(values: INewEmployeeValues) {
    const url = `/employees`;
    const headers = new HttpHeaders({
      Authorization: this.storageService.get('token'),
    });

    return this.http.post(url, values, { headers });
  }

  public getInterviews(requestId: string) {
    const url = `/interviews/request/${requestId}`;
    const headers = new HttpHeaders({
      Authorization: this.storageService.get('token'),
    });

    return this.http.get(url, { headers });
  }

  public changeInterviewDate(interviewId: string, newDate: Date) {
    const url = `/interviews/${interviewId}/date`;
    const headers = new HttpHeaders({
      Authorization: this.storageService.get('token'),
    });

    return this.http.patch(url, { newDate }, { headers });
  }
}
