import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RecruiterGuardService } from './recruiter-guard.service';
import { RecruiterHomeComponent } from './recruiter-home/recruiter-home.component';
import { RecruiterNewRequestsComponent } from './requests/new-requests/new-requests.component';
import { RecruiterAllRequestsComponent } from './requests/all-requests/all-requests.component';
import { RecruiterDatabaseComponent } from './database/database.component';
import { RecruiterProfileComponent } from './profile/profile.component';
import { RecruiterNewRequestComponent } from './requests/new-request/new-request.component';
import { RecruiterPersonDetailComponent } from './database/person-detail/person-detail.component';
import { RecruiterRequestComponent } from './requests/request/request.component';
import { RecruiterAddCandidateComponent } from './requests/request/add-candidate/add-candidate.component';
import { RecruiterNewPersonComponent } from './new-person/new-person.component';

const recruiterRoutes: Routes = [
  {
    path: '',
    component: RecruiterHomeComponent,
    canActivate: [RecruiterGuardService],
    children: [
      {
        path: 'person/:id',
        component: RecruiterPersonDetailComponent,
      },
      {
        path: 'new-requests/:id',
        component: RecruiterNewRequestComponent,
      },
      {
        path: 'all-requests/:id/add-candidate',
        component: RecruiterAddCandidateComponent,
      },
      {
        path: 'all-requests/:id',
        component: RecruiterRequestComponent,
      },
      {
        path: 'new-requests',
        component: RecruiterNewRequestsComponent,
      },
      {
        path: 'all-requests',
        component: RecruiterAllRequestsComponent,
      },
      {
        path: 'database/add',
        component: RecruiterNewPersonComponent,
      },
      {
        path: 'database',
        component: RecruiterDatabaseComponent,
      },
      {
        path: 'profile',
        component: RecruiterProfileComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(recruiterRoutes)],
  exports: [RouterModule],
})
export class RecruiterRoutingModule {}
