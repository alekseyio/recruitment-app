import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { DirectorRequestsService } from '../my-requests/director-requests.service';
import { InterviewsService } from '../interviews/interviews.service';
import { SidebarService } from '../../shared/services/sidebar.service';

@Component({
  selector: 'app-director-home',
  templateUrl: './home.component.html',
  providers: [DirectorRequestsService]
})
export class DirectorHomeComponent implements OnInit {
  constructor(
    private router: Router,
    private interviewsService: InterviewsService,
    private sidebarService: SidebarService
  ) {}

  ngOnInit() {
    this._getCount();

    if (this.router.url === '/director') {
      this.router.navigateByUrl('/director/my-requests');
    }
  }

  private _getCount() {
    this.interviewsService
      .getInterviewCount()
      .subscribe(this.sidebarService.setCount);

    setTimeout(() => this._getCount(), 300000); // Every 5 min
  }
}
