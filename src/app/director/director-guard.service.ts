import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { map, delay } from 'rxjs/operators';

import { AuthService } from '../auth/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class DirectorGuardService implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  canActivate() {
    return this.authService.user$.pipe(
      delay(100),
      map(user => {
        if (!user || user.role !== 'director') {
          return this.router.createUrlTree(['/']);
        }

        return true;
      })
    );
  }
}
