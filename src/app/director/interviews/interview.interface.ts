export interface IInterview {
  requestId: string;
  interviewId: string;
  fullName: string;
  phone: string;
  comment?: string;
  interviewDate: Date;
  position: string;
}
