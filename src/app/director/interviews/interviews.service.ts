import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { StorageService } from '../../services/storage.service';
import { Request } from 'src/app/request.interface';
import { Interview } from 'src/app/request.interface';
import { IFormValues } from './interview-detail/form-values.interface';
import { IInterview } from '../../interfaces/Interview.interface';

@Injectable({
  providedIn: 'root',
})
export class InterviewsService {
  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {}

  public fetchInterviewsForRequest(interviewId: string) {
    const url = `/interviews/${interviewId}/director`;
    const headers = new HttpHeaders({
      Authorization: this.storageService.get('token'),
    });

    return this.http.get(url, { headers });
  }

  public setInterviewResult = (interviewId: string, values) => {
    const url = `/interviews/${interviewId}/result`;
    const headers = new HttpHeaders({
      Authorization: this.storageService.get('token'),
    });

    return this.http.patch(url, values, { headers });
  };

  public getInterviewCount(): Observable<number> {
    const url = '/director/interviews/count';
    const headers = new HttpHeaders({
      Authorization: this.storageService.get('token'),
    });

    return this.http.get<number>(url, { headers });
  }

  public fetchInterviews(): Observable<IInterview[]> {
    const url = '/interviews/director';
    const headers = new HttpHeaders({
      Authorization: this.storageService.get('token'),
    });

    return this.http.get<IInterview[]>(url, { headers });
  }
}
