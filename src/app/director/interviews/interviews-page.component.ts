import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

import { InterviewsService } from './interviews.service';
import { IInterview } from '../../interfaces/Interview.interface';

@Component({
  selector: 'app-director-interviews-page',
  templateUrl: './interviews-page.component.html',
  styleUrls: ['./interviews-page.component.scss'],
})
export class DirectorInterviewsPageComponent implements OnInit {
  public isFetching: boolean;
  public interviews: IInterview[];

  constructor(
    private router: Router,
    private interviewsService: InterviewsService
  ) {}

  ngOnInit() {
    this.isFetching = true;

    this.interviewsService
      .fetchInterviews()
      .subscribe(this.onFetchInterviews, this.onFetchInterviewsError);
  }

  onFetchInterviews = (interviews: IInterview[]): void => {
    this.interviews = interviews;
    this.isFetching = false;
  };

  onFetchInterviewsError = (err: HttpErrorResponse): void => {
    this.isFetching = false;

    console.log('DirectorInterviewsPageComponent#onFetchInterviewsError', err);
  };

  onInterviewTitleClick = (interview: IInterview): void => {
    console.log(interview);
    this.router.navigate(['director', 'interviews', interview.id]);
  };

  public getInterviewResult = ({ result }: IInterview) => {
    if (result === 'success') {
      return 'Принят на работу';
    } else if (result === 'fail') {
      return 'Не принят на работу';
    }
  };
}
