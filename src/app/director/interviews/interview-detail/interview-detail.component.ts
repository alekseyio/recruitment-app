import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { FormGroup, FormControl } from '@angular/forms';
import { switchMap } from 'rxjs/operators';

import { InterviewsService } from '../interviews.service';
import { Request, Interview } from 'src/app/request.interface';
import { IFormValues } from './form-values.interface';
import { IInterview } from '../../../interfaces/Interview.interface';

interface IFormErrors {
  result?: string;
  comment?: string;
}

@Component({
  selector: 'app-director-interview-detail',
  templateUrl: './interview-detail.component.html',
  styleUrls: ['./interview-detail.component.scss'],
})
export class DirectorInterviewDetailComponent implements OnInit {
  public isFetching: boolean;
  public interview: IInterview;
  public form: FormGroup = new FormGroup({
    result: new FormControl(''),
    comment: new FormControl(''),
  });
  public errors: IFormErrors = {};

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private interviewsService: InterviewsService
  ) {}

  ngOnInit() {
    this.isFetching = true;

    this.route.paramMap
      .pipe(
        switchMap((params: ParamMap) => {
          const interviewId = params.get('id');

          return this.interviewsService.fetchInterviewsForRequest(interviewId);
        })
      )
      .subscribe(this.handleFetchSuccess, this.handleFetchError);
  }

  private handleFetchSuccess = (interview: IInterview): void => {
    this.interview = interview;
    this.isFetching = false;
  };

  private handleFetchError = (err: HttpErrorResponse): void => {
    this.isFetching = false;

    if (err.status === 404) {
      this.router.navigate(['director', 'interviews']);
    } else {
      console.log('DirectorInterviewDetailComponent#handleFetchError', err);
    }
  };

  public onSubmit = () => {
    this.errors = {};

    this.interviewsService
      .setInterviewResult(this.interview.id, this.form.value)
      .subscribe(this.onSetInterviewResult, this.onSetInterviewResultErr);
  };

  private onSetInterviewResult = (interview: IInterview) => {
    this.interview = interview;
  };

  private onSetInterviewResultErr = (err: HttpErrorResponse) => {
    if (err.status === 400) {
      this.errors = err.error.errors;
    } else {
      console.log(err);
    }
  };

  // public handleSubmit() {
  //   this.errors = {};

  //   const formValues: IFormValues = this.form.value;

  //   this.interviewsService
  //     .setInterviewResult(this.request.id, this.interview._id, formValues)
  //     .subscribe(this.handleSetInterviewSuccess, this.handleSetInterviewError);
  // }

  // private handleSetInterviewSuccess = (response: {
  //   request: Request;
  //   interview: Interview;
  // }): void => {
  //   this.request = response.request;
  //   this.interview = response.interview;
  // };

  // private handleSetInterviewError = (err: HttpErrorResponse): void => {
  //   if (err.status === 400) {
  //     this.errors = err.error.errors;
  //   } else {
  //     console.log(
  //       'DirectorInterviewDetailComponent#handleSetInterviewError',
  //       err
  //     );
  //   }
  // };

  public get interviewResult(): string {
    const { result } = this.interview;

    switch (result) {
      case 'success': {
        return 'Принят на работу';
      }
      case 'fail': {
        return 'Не принят работу';
      }
    }

    return '';
  }
}
