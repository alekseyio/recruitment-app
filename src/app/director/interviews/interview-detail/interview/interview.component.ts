import { Component, Input } from '@angular/core';

import { IInterview } from '../../../../interfaces/Interview.interface';

@Component({
  selector: 'app-director-interview',
  templateUrl: './interview.component.html',
  styleUrls: ['./interview.component.scss'],
})
export class DirectorInterviewComponent {
  @Input() interview: IInterview;
}
