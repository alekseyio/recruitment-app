import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DirectorGuardService } from './director-guard.service';
import { DirectorHomeComponent } from './home/home.component';
import { DirectorNewRequestComponent } from './new-request/new-request.component';
import { DirectorMyRequestsComponent } from './my-requests/my-requests.component';
import { DirectorProfileComponent } from './profile/profile.component';
import { DirectorEmployeesPageComponent } from './employees/employees-page.component';
import { DirectorRequestDetailsComponent } from './my-requests/request-details/request-details.component';
import { DirectorInterviewDetailComponent } from './interviews/interview-detail/interview-detail.component';
import { DirectorInterviewsPageComponent } from './interviews/interviews-page.component';

const directorRoutes: Routes = [
  {
    path: '',
    component: DirectorHomeComponent,
    canActivate: [DirectorGuardService],
    children: [
      {
        path: 'new-request',
        component: DirectorNewRequestComponent
      },
      {
        path: 'my-requests/:id',
        component: DirectorRequestDetailsComponent
      },
      {
        path: 'my-requests',
        component: DirectorMyRequestsComponent
      },
      {
        path: 'interviews/:id',
        component: DirectorInterviewDetailComponent
      },
      {
        path: 'interviews',
        component: DirectorInterviewsPageComponent
      },
      {
        path: 'employees',
        component: DirectorEmployeesPageComponent
      },
      {
        path: 'profile',
        component: DirectorProfileComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(directorRoutes)],
  exports: [RouterModule]
})
export class DirectorRoutingModule {}
