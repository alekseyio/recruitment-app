import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '../shared/shared.module';
import { DirectorRoutingModule } from './director-routing.module';
import { DirectorHomeComponent } from './home/home.component';
import { DirectorProfileComponent } from './profile/profile.component';
import { DirectorNewRequestComponent } from './new-request/new-request.component';
import { DirectorAddCompany } from './new-request/add-company/add-company.component';
import { DirectorMyRequestsComponent } from './my-requests/my-requests.component';
import { DirectorEmployeesPageComponent } from './employees/employees-page.component';
import { DirectorNewRequestFormComponent } from './new-request/new-request-form/new-request-form.component';
import { DirectorRequestsListComponent } from './my-requests/requests-list/requests-list.component';
import { DirectorRequestComponent } from './my-requests/requests-list/request-item/request-item.component';
import { DirectorRequestDetailsComponent } from './my-requests/request-details/request-details.component';
import { DirectorRequestInfoComponent } from './my-requests/request-details/request-info/request-info.component';
import { DirectorRequestInterviewsComponent } from './my-requests/request-details/request-interviews/request-interviews.component';
import { DirectorInterviewDetailComponent } from './interviews/interview-detail/interview-detail.component';
import { DirectorInterviewComponent } from './interviews/interview-detail/interview/interview.component';
import { DirectorInterviewsPageComponent } from './interviews/interviews-page.component';
import { RequestsTableComponent } from './my-requests/requests-table/requests-table.component';

@NgModule({
  declarations: [
    DirectorHomeComponent,
    DirectorProfileComponent,
    DirectorNewRequestComponent,
    DirectorAddCompany,
    DirectorNewRequestFormComponent,
    DirectorMyRequestsComponent,
    DirectorRequestsListComponent,
    DirectorRequestComponent,
    DirectorEmployeesPageComponent,
    DirectorRequestDetailsComponent,
    DirectorRequestInfoComponent,
    DirectorRequestInterviewsComponent,
    DirectorInterviewDetailComponent,
    DirectorInterviewComponent,
    DirectorInterviewsPageComponent,
    RequestsTableComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
    DirectorRoutingModule
  ]
})
export class DirectorModule {}
