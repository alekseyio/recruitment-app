import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

import {
  EmployeeService,
  IResponse,
  IEmployee,
  ICompany,
} from './employee.service';
import { NotificationService } from '../../notifications/notification.service';

@Component({
  selector: 'app-director-employees',
  templateUrl: './employees-page.component.html',
  styleUrls: ['./employees-page.component.scss'],
})
export class DirectorEmployeesPageComponent implements OnInit {
  public isFetching: boolean;
  public employees: IEmployee[];
  public company: ICompany;

  constructor(
    private router: Router,
    private employeeService: EmployeeService,
    private notificationService: NotificationService
  ) {}

  ngOnInit() {
    this.isFetching = true;

    this.employeeService
      .fetchEmployees()
      .subscribe(this.handleFetchEmployee, this.handleFetchEmployeeError);
  }

  private handleFetchEmployee = (response: IResponse): void => {
    console.log(response);
    this.employees = response.employees;
    this.company = response.company;
    this.isFetching = false;
  };

  private handleFetchEmployeeError = (err: HttpErrorResponse): void => {
    this.isFetching = false;

    if (err.status === 403) {
      this.router.navigate(['director', 'new-request']);
      this.notificationService.show(
        'Для просмотра списка сотрудников сперва создайте компанию'
      );
    } else {
      console.log(
        'DirectorEmployeesPageComponent#handleFetchEmployeeError',
        err
      );
    }
  };
}
