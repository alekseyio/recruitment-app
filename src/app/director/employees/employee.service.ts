import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { StorageService } from '../../services/storage.service';
import { IEmployedPerson } from '../../interfaces/Person.interface';

export interface IResponse {
  company: ICompany;
  employees: IEmployee[];
}

export interface ICompany {
  brand: string;
  address: string;
  number: number;
}

export interface IEmployee {
  company: string;
  person: IEmployedPerson;
  position: string;
  schedule: string;
}

@Injectable({
  providedIn: 'root',
})
export class EmployeeService {
  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {}

  public fetchEmployees(): Observable<IResponse> {
    const url = '/employees';
    const headers = new HttpHeaders({
      Authorization: this.storageService.get('token'),
    });

    return this.http.get<IResponse>(url, { headers });
  }
}
