import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

import { AddCompanyService } from './add-company.service';
import { AuthService } from '../../../auth/services/auth.service';
import { NotificationService } from '../../../notifications/notification.service';

enum State {
  Warning,
  Form
}

interface FormErrors {
  brand?: string;
  objectNumber?: string;
  subway?: string;
  subwayLine?: string;
  address?: string;
}

@Component({
  selector: 'app-director-add-company',
  templateUrl: './add-company.component.html',
  styleUrls: ['./add-company.component.scss'],
  providers: [AddCompanyService]
})
export class DirectorAddCompany {
  public state = State.Warning;
  public readonly STATES = State;
  public form: FormGroup;
  public errors: FormErrors;

  constructor(
    private addCompanyService: AddCompanyService,
    private authService: AuthService,
    private notificationService: NotificationService
  ) {}

  ngOnInit() {
    this.initForm();
    this.initErrors();
  }

  private initForm(): void {
    this.form = new FormGroup({
      brand: new FormControl(''),
      objectNumber: new FormControl(''),
      subway: new FormControl(''),
      subwayLine: new FormControl(0),
      address: new FormControl('')
    });
  }

  private initErrors(): void {
    this.errors = {};
  }

  public handleWarningButtonClick(): void {
    this.state = State.Form;
  }

  public handleSubmit(): void {
    this.initErrors();

    this.addCompanyService
      .addCompany(this.form.value)
      .subscribe(
        this.handleRequest.bind(this),
        this.handleRequestError.bind(this)
      );
  }

  private handleRequest(): void {
    this.authService.fetchUser();
    this.notificationService.show('Объект успешно создан');
  }

  private handleRequestError(errors): void {
    this.errors = errors;
  }
}
