import { Injectable } from '@angular/core';
import {
  HttpHeaders,
  HttpClient,
  HttpErrorResponse
} from '@angular/common/http';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Company } from './company.interface';
import { StorageService } from '../../../services/storage.service';
import { AuthService } from '../../../auth/services/auth.service';

@Injectable()
export class AddCompanyService {
  private readonly url = '/director/add-company';

  constructor(
    private storageService: StorageService,
    private http: HttpClient,
    private authService: AuthService,
    private router: Router
  ) {}

  public addCompany(company: Company) {
    const token = this.storageService.get('token');
    const headers = new HttpHeaders({ Authorization: token });

    return this.http
      .post(this.url, company, { headers })
      .pipe(catchError(this.handleRequestError.bind(this)));
  }

  private handleRequestError(err: HttpErrorResponse) {
    if (err.status === 400) {
      return throwError(err.error.errors);
    } else if (err.status === 401) {
      this.authService.setUser(null);
      this.storageService.clear('token');
      this.router.navigateByUrl('/auth/token');
    } else {
      this.router.navigateByUrl('/server-offline');
    }
  }
}
