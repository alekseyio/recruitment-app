export interface Company {
  brand: string;
  objectNumber: number;
  subway: string;
  address: string;
}
