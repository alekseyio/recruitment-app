import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';
import { catchError } from 'rxjs/operators';

import { StorageService } from '../../../services/storage.service';
import { throwError } from 'rxjs';

interface Request {
  title: string;
  position: string;
  schedule: string;
  qty: number;
  comment?: string;
}

export interface RequestError {
  status: number;
  errors: {
    title?: string;
    position?: string;
    schedule?: string;
    qty?: string;
    comment?: string;
    message?: string;
  };
}

@Injectable()
export class NewRequestService {
  private URL = '/requests';

  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {}

  public createRequest(request: Request) {
    const token = this.storageService.get('token');
    const headers = new HttpHeaders({
      Authorization: token
    });

    return this.http
      .post(this.URL, request, { headers })
      .pipe(catchError(this.handleRequestError.bind(this)));
  }

  private handleRequestError(err: HttpErrorResponse) {
    let error: RequestError;

    if (err.status === 400) {
      error = {
        status: 400,
        errors: err.error.errors
      };

      return throwError(error);
    }

    if (err.status === 403) {
      error = {
        status: 403,
        errors: {
          message:
            'Вы не можете создавать заявки с учетной записи, не закрепленной за каким-либо объектом'
        }
      };

      return throwError(error);
    }

    error = {
      status: 500,
      errors: {
        message: 'Произошла ошибка на стороне сервера'
      }
    };

    return throwError(error);
  }
}
