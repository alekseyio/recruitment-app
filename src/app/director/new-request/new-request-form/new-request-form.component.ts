import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { schedule } from './schedule';
import { NewRequestService, RequestError } from './new-request.service';
import { NotificationService } from '../../../notifications/notification.service';

interface Errors {
  title?: string;
  position?: string;
  schedule?: string;
  rate?: string;
  qty?: string;
  comment?: string;
}

@Component({
  selector: 'app-director-new-request-form',
  templateUrl: './new-request-form.component.html',
  styleUrls: ['./new-request-form.component.scss'],
  providers: [NewRequestService]
})
export class DirectorNewRequestFormComponent implements OnInit {
  public form: FormGroup;
  public errors: Errors;

  constructor(
    private newRequestService: NewRequestService,
    private notificationService: NotificationService,
    private router: Router
  ) {}

  ngOnInit() {
    this.initForm();
    this.initErrors();
  }

  private initForm(): void {
    this.form = new FormGroup({
      title: new FormControl(''),
      position: new FormControl(''),
      schedule: new FormControl(null),
      rate: new FormControl(''),
      qty: new FormControl(1),
      comment: new FormControl('')
    });
  }

  private initErrors(): void {
    this.errors = {};
  }

  get schedule(): string[] {
    return schedule;
  }

  public handleSubmit(): void {
    this.initErrors();

    const request = { ...this.form.value };

    if (!request.comment.length) {
      request.comment = undefined;
    }

    this.newRequestService
      .createRequest(request)
      .subscribe(
        this.handleRequest.bind(this),
        this.handleRequestError.bind(this)
      );
  }

  private handleRequest() {
    this.notificationService.show('Заявка успешно создана');
    this.router.navigateByUrl('/director/my-requests');
  }

  private handleRequestError(err: RequestError) {
    if (err.status === 400) {
      this.errors = err.errors;
    } else if (err.status === 403) {
      this.notificationService.show(err.errors.message);
    } else if (err.status === 500) {
      this.notificationService.show(err.errors.message);
      this.router.navigateByUrl('/server-offline');
    }
  }
}
