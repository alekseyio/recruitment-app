import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { AuthService } from '../../auth/services/auth.service';

@Component({
  selector: 'app-director-new-request',
  templateUrl: './new-request.component.html',
  styleUrls: ['./new-request.component.scss']
})
export class DirectorNewRequestComponent implements OnInit, OnDestroy {
  private userSubscription: Subscription;
  public hasCompany: boolean;

  constructor(private authService: AuthService) {}

  ngOnInit() {
    this.userSubscription = this.authService.user$.subscribe(user => {
      this.hasCompany = user.company ? true : false;
    });
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }
}
