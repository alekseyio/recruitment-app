import { Component, Input } from '@angular/core';

import { Request } from '../../../request.interface';

@Component({
  selector: 'app-director-requests-list',
  templateUrl: './requests-list.component.html',
  styleUrls: ['./requests-list.component.scss']
})
export class DirectorRequestsListComponent {
  @Input() requests: Request[];
}
