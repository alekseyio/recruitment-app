import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

import { Request } from '../../../../request.interface';

@Component({
  selector: 'app-director-request',
  templateUrl: './request-item.component.html',
  styleUrls: ['./request-item.component.scss']
})
export class DirectorRequestComponent {
  @Input() request: Request;

  constructor(private router: Router) {}

  public handleDetailsButtonClick(): void {
    this.router.navigate(['director', 'my-requests', this.request.id]);
  }
}
