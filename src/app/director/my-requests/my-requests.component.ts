import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { DirectorRequestsService } from './director-requests.service';
import { Request } from 'src/app/request.interface';

@Component({
  selector: 'app-director-my-requests',
  templateUrl: './my-requests.component.html',
  styleUrls: ['./my-requests.component.scss']
})
export class DirectorMyRequestsComponent implements OnInit, OnDestroy {
  private requestsSubscription: Subscription;
  public requests: Request[];
  public isFetching: boolean;

  constructor(
    private directorRequestsService: DirectorRequestsService,
    private router: Router
  ) {}

  ngOnInit() {
    this.requestsSubscription = this.directorRequestsService.requests$.subscribe(
      requests => (this.requests = requests)
    );

    this.isFetching = true;

    this.directorRequestsService.fetchRequests(() => {
      this.isFetching = false;
    });
  }

  ngOnDestroy() {
    this.requestsSubscription.unsubscribe();
  }

  public handleCreateButtonClick(): void {
    this.router.navigate(['director', 'new-request']);
  }
}
