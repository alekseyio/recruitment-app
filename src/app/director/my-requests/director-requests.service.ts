import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse,
} from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { catchError, delay } from 'rxjs/operators';

import { Request } from 'src/app/request.interface';
import { AuthService } from '../../auth/services/auth.service';
import { StorageService } from '../../services/storage.service';
import { NotificationService } from '../../notifications/notification.service';
import { IInterview } from '../../interfaces/Interview.interface';

@Injectable()
export class DirectorRequestsService {
  private _requests = new BehaviorSubject<Request[]>([]);
  public requests$ = this._requests.asObservable();

  constructor(
    private http: HttpClient,
    private authService: AuthService,
    private storageService: StorageService,
    private router: Router,
    private notificationService: NotificationService
  ) {}

  private get requests(): Request[] {
    return this._requests.getValue();
  }

  private set requests(requests: Request[]) {
    this._requests.next(requests);
  }

  public fetchRequests(cb: () => void) {
    const user = this.authService.getUser();
    const token = this.storageService.get('token');
    const url = `/requests/user/${user.id}`;
    const headers = new HttpHeaders({
      Authorization: token,
    });

    return this.http
      .get(url, { headers })
      .pipe(delay(500), catchError(this.handleRequestError.bind(this)))
      .subscribe((response: { requests: Request[] }) => {
        this.requests = response.requests;

        cb();
      });
  }

  private handleRequestError(err: HttpErrorResponse) {
    if (err.status === 404) {
      this.notificationService.show(
        'Данного пользователя больше нет в системе'
      );
      this.storageService.clearStorage();
      this.authService.setUser(null);
      this.router.navigateByUrl('/auth/login');
    } else {
      this.notificationService.show('Произошла ошибка на стороне сервера');
      this.router.navigateByUrl('/server-offline');
    }
  }

  public fetchRequest(id: string): Observable<Request> {
    const url = `/requests/${id}`;
    const headers = new HttpHeaders({
      Authorization: this.storageService.get('token'),
    });

    return this.http.get<Request>(url, { headers });
  }

  public deleteRequest(id: string) {
    const url = `/director/requests/${id}`;
    const headers = new HttpHeaders({
      Authorization: this.storageService.get('token'),
    });

    return this.http.delete(url, { headers });
  }

  public fetchInterviewsForRequest(requestId: string) {
    const url = `/interviews/request/${requestId}`;
    const headers = new HttpHeaders({
      Authorization: this.storageService.get('token'),
    });

    return this.http.get<IInterview[]>(url, { headers });
  }
}
