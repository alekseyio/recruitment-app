import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

import { Interview } from '../../../../request.interface';

@Component({
  selector: 'app-director-request-interviews',
  templateUrl: './request-interviews.component.html',
  styleUrls: ['./request-interviews.component.scss'],
})
export class DirectorRequestInterviewsComponent {
  @Input() interviews: Interview[];

  constructor(private router: Router) {}

  public redirectToInterviewPage(interviewId: string, requestId: string): void {
    this.router.navigate([
      'director',
      'interviews',
      interviewId,
      {
        requestId: requestId,
      },
    ]);
  }

  public interviewExpired(dateString: string): boolean {
    return Date.now() > new Date(dateString).getTime();
  }
}
