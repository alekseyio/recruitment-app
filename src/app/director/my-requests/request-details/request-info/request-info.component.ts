import { Component, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Request } from '../../../../request.interface';
import { DirectorRequestsService } from '../../director-requests.service';
import { NotificationService } from 'src/app/notifications/notification.service';

@Component({
  selector: 'app-director-request-info',
  templateUrl: './request-info.component.html',
  styleUrls: ['./request-info.component.scss'],
})
export class DirectorRequestInfoComponent {
  @Input() request: Request;
  public modalShown: boolean = false;
  public deleteBtnDisabled = true;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private requestsService: DirectorRequestsService,
    private notificationService: NotificationService
  ) {}

  public get status(): string {
    const { status } = this.request;

    switch (status) {
      case 'new': {
        return 'в обработке';
      }
      case 'processing': {
        return 'подбор кандидатов';
      }
      default: {
        return 'не известно';
      }
    }
  }

  public handleDeleteClick = (): void => {
    this.modalShown = true;
  };

  public handleModalConfirm = (): void => {
    this.modalShown = false;

    if (this.deleteBtnDisabled) {
      return;
    }

    this.requestsService.deleteRequest(this.request.id).subscribe(() => {
      this.router.navigate(['..'], {
        relativeTo: this.route,
      });
      this.notificationService.show('Заявка успешно удалена');
    });
  };

  public handleModalClose = (): void => {
    this.modalShown = false;
  };
}
