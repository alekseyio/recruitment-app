import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { switchMap } from 'rxjs/operators';

import { DirectorRequestsService } from '../director-requests.service';
import { Request } from 'src/app/request.interface';
import { IInterview } from '../../../interfaces/Interview.interface';

@Component({
  selector: 'app-director-request-details',
  templateUrl: 'request-details.component.html',
  styleUrls: ['./request-details.component.scss'],
})
export class DirectorRequestDetailsComponent implements OnInit {
  public isFetching: boolean;
  public request: Request;
  public interviews: IInterview[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private requestsService: DirectorRequestsService
  ) {}

  ngOnInit() {
    this.isFetching = true;

    this.route.paramMap
      .pipe(
        switchMap((params: ParamMap) =>
          this.requestsService.fetchRequest(params.get('id'))
        )
      )
      .subscribe(this.handleSuccessFetch, this.handleErrorFetch);
  }

  private handleSuccessFetch = (request: Request): void => {
    this.request = request;
    this.fetchInterviews();
  };

  private handleErrorFetch = (err: HttpErrorResponse): void => {
    this.isFetching = false;

    if (err.status === 404) {
      this.router.navigate(['..'], {
        relativeTo: this.route,
      });
    } else {
      console.log('DirectorRequestDetailsComponent#handleErrorFetch', err);
    }
  };

  private fetchInterviews() {
    this.requestsService
      .fetchInterviewsForRequest(this.request.id)
      .subscribe(this.onFetchInterviews, this.onFetchInterviewsErr);
  }

  private onFetchInterviews = (interviews: IInterview[]) => {
    this.interviews = interviews;
    this.isFetching = false;
  };

  private onFetchInterviewsErr = (err: HttpErrorResponse) => {
    this.isFetching = false;
    console.log(err);
  };
}
