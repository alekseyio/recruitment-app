import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppHomeComponent } from './app-home.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ServerOfflineComponent } from './server-offline/server-offline.component';

const appRoutes: Routes = [
  {
    path: '',
    component: AppHomeComponent,
    pathMatch: 'full'
  },
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then(mod => mod.AuthModule)
  },
  {
    path: 'director',
    loadChildren: () =>
      import('./director/director.module').then(mod => mod.DirectorModule)
  },
  {
    path: 'recruiter',
    loadChildren: () =>
      import('./recruiter/recruiter.module').then(mod => mod.RecruiterModule)
  },
  {
    path: 'server-offline',
    component: ServerOfflineComponent
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
