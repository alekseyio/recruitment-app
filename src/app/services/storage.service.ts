import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  public set(key: string, value: any): void {
    window.localStorage.setItem(key, JSON.stringify(value));
  }

  public get(key: string): any {
    const value = window.localStorage.getItem(key);

    return JSON.parse(value);
  }

  public clear(key: string): void {
    window.localStorage.removeItem(key);
  }

  public clearStorage(): void {
    window.localStorage.clear();
  }
}
