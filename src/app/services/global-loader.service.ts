import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GlobalLoaderService {
  private _shown = new BehaviorSubject<boolean>(false);
  public shown$ = this._shown.asObservable();

  public show(): void {
    this._shown.next(true);
  }

  public hide(): void {
    this._shown.next(false);
  }
}
