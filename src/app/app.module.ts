import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { registerLocaleData } from '@angular/common';
import localeRu from '@angular/common/locales/ru';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { NotificationsModule } from './notifications/notifications.module';
import { SharedModule } from './shared/shared.module';
import { AppComponent } from './app.component';
import { AppHomeComponent } from './app-home.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ServerOfflineComponent } from './server-offline/server-offline.component';
import { LoaderComponent } from './loader/loader.component';
import { UrlPrefixInterceptor } from './url-prefix.interceptor';

registerLocaleData(localeRu);

@NgModule({
  declarations: [
    AppComponent,
    AppHomeComponent,
    PageNotFoundComponent,
    ServerOfflineComponent,
    LoaderComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NotificationsModule,
    SharedModule,
    AppRoutingModule,
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: UrlPrefixInterceptor, multi: true},
  ],
  bootstrap: [AppComponent],
  exports: []
})
export class AppModule {}
