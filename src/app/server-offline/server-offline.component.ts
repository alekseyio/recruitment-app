import { Component } from '@angular/core';

@Component({
  selector: 'app-server-offline',
  templateUrl: './server-offline.component.html'
})
export class ServerOfflineComponent {
  public errorCode = 500;
  public errorMessage =
    'Ошибка на стороне сервера. Попробуйте перезагрузить страницу';

  public handleClick(): void {
    window.location.href = '/';
  }
}
