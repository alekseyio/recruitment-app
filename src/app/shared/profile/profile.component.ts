import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent {
  @Input() userName: {
    firstName: string;
    lastName: string;
    middleName: string;
  };
  @Input() email: string;

  get fullName(): string {
    return `${this.userName.lastName} ${this.userName.firstName} ${this.userName.middleName}`;
  }
}
