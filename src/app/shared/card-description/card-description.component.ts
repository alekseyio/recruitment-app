import { Component } from '@angular/core';

@Component({
  selector: 'app-card-description',
  template: `
    <p>
      <ng-content></ng-content>
    </p>
  `,
  styleUrls: ['./card-description.component.scss']
})
export class CardDescriptionComponent {}
