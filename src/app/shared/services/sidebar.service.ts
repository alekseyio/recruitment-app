import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {
  private sidebarShown = new BehaviorSubject<boolean>(false);
  public sidebarShown$ = this.sidebarShown.asObservable();
  private _count = new BehaviorSubject<number>(0);
  public count$ = this._count.asObservable();

  openSidebar() {
    this.sidebarShown.next(true);
  }

  closeSidebar() {
    this.sidebarShown.next(false);
  }

  public setCount = (count: number): void => {
    this._count.next(count);
  };
}
