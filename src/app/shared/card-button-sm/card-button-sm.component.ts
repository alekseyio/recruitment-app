import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-card-button-sm',
  templateUrl: './card-button-sm.component.html',
  styleUrls: ['./card-button-sm.component.scss']
})
export class CardButtonSmallComponent {
  @Input() styles = {};
  @Output() clicked: EventEmitter<MouseEvent> = new EventEmitter();

  handleClick(e: MouseEvent) {
    this.clicked.emit(e);
  }
}
