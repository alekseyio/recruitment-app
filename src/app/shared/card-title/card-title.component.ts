import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-card-title',
  template: `
    <h3 [ngStyle]="styles">
      <ng-content></ng-content>
    </h3>
  `,
  styleUrls: ['./card-title.component.scss']
})
export class CardTitleComponent {
  @Input() styles: {};
}
