import { Component, Output, EventEmitter, Input } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [
        style({ opacity: '0' }),
        animate('0.2s ease', style({ opacity: '1' }))
      ]),
      transition(':leave', [animate('0.2s ease', style({ opacity: '0' }))])
    ]),
    trigger('slideUpDown', [
      transition(':enter', [
        style({ bottom: '-32rem' }),
        animate('0.2s ease', style({ bottom: '-7rem' }))
      ]),
      transition(':leave', [animate('0.2s ease', style({ bottom: '-32rem' }))])
    ])
  ]
})
export class ModalComponent {
  @Input() shown: boolean;
  @Output() close = new EventEmitter();
  @Output() confirm = new EventEmitter();

  public closeModal(): void {
    this.close.emit();
  }

  public confirmAction(): void {
    this.confirm.emit();
  }
}
