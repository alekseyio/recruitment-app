import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

import { SidebarService } from '../services/sidebar.service';
import { AuthService } from '../../auth/services/auth.service';
import { User } from '../../auth/interfaces/User.interface';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  public currentTime = new Date();
  private intervalId: number;
  private userSubscription: Subscription;
  public user: User;
  public profileLink: string;

  constructor(
    private sidebarService: SidebarService,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.intervalId = window.setInterval(
      () => this.setCurrentTime(),
      60 * 1000
    );

    this.userSubscription = this.authService.user$
      .pipe(take(1))
      .subscribe(user => {
        this.user = user;
        this.profileLink = `/${this.user.role}/profile`;
      });
  }

  private setCurrentTime(): void {
    this.currentTime = new Date();
  }

  public openSidebar(): void {
    this.sidebarService.openSidebar();
  }

  get userName(): string {
    if (!this.user) {
      return 'No username';
    }

    return `${this.user.firstName} ${this.user.lastName}`;
  }

  ngOnDestroy() {
    window.clearInterval(this.intervalId);

    this.userSubscription.unsubscribe();
  }
}
