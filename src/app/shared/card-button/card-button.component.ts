import { Component, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-card-button',
  templateUrl: './card-button.component.html',
  styleUrls: ['./card-button.component.scss'],
})
export class CardButtonComponent {
  @Input() styles = {};
  @Input() disabled: boolean = false;
  @Output() clicked: EventEmitter<MouseEvent> = new EventEmitter();

  handleClick(e: MouseEvent) {
    this.clicked.emit(e);
  }
}
