import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { LayoutComponent } from './layout/layout.component';
import { CardComponent } from './card/card.component';
import { CardButtonComponent } from './card-button/card-button.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { CardTitleComponent } from './card-title/card-title.component';
import { CardDescriptionComponent } from './card-description/card-description.component';
import { SectionTitleComponent } from './section-title/section-title.component';
import { LocalLoaderComponent } from './loader/loader.component';
import { ProfileComponent } from './profile/profile.component';
import { ModalComponent } from './modal/modal.component';
import { CheckboxComponent } from './checkbox/checkbox.component';
import { CardButtonSmallComponent } from './card-button-sm/card-button-sm.component';
import { ButtonComponent } from './button/button.component';
import {NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';
import { UserDropdownComponent } from './user-dropdown/user-dropdown.component';

@NgModule({
  declarations: [
    HeaderComponent,
    SidebarComponent,
    LayoutComponent,
    CardComponent,
    CardButtonComponent,
    CardButtonSmallComponent,
    CardTitleComponent,
    CardDescriptionComponent,
    ErrorPageComponent,
    SectionTitleComponent,
    LocalLoaderComponent,
    ProfileComponent,
    ModalComponent,
    CheckboxComponent,
    ButtonComponent,
    UserDropdownComponent,
  ],
  imports: [CommonModule, RouterModule, FormsModule, NgbDropdownModule],
  exports: [
    LayoutComponent,
    CardComponent,
    CardButtonComponent,
    CardButtonSmallComponent,
    CardTitleComponent,
    CardDescriptionComponent,
    ErrorPageComponent,
    SectionTitleComponent,
    LocalLoaderComponent,
    ProfileComponent,
    ModalComponent,
    CheckboxComponent,
    ButtonComponent,
  ],
})
export class SharedModule {}
