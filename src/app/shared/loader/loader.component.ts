import { Component } from '@angular/core';

@Component({
  selector: 'app-local-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LocalLoaderComponent {}
