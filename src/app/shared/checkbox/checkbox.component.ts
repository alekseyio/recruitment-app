import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss']
})
export class CheckboxComponent {
  @Input() name: string;
  @Input() label: string = 'Label';
  @Input() value: boolean;
  @Output() changed = new EventEmitter<boolean>();
  public id = Date.now();

  public handleChange(e: Event): void {
    const el = e.target as HTMLInputElement;

    this.changed.emit(el.checked);
  }
}
