import {Component, OnInit} from '@angular/core';
import {take} from 'rxjs/operators';
import {Router} from '@angular/router';
import {AuthService} from '../../auth/services/auth.service';
import {StorageService} from '../../services/storage.service';
import {User} from '../../auth/interfaces/User.interface';

@Component({
  selector: 'app-user-dropdown',
  templateUrl: './user-dropdown.component.html',
  styleUrls: ['./user-dropdown.component.scss']
})
export class UserDropdownComponent implements OnInit {
  public user: User;

  constructor(
    private router: Router,
    private storageService: StorageService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.authService.user$.pipe(take(1)).subscribe(user => (this.user = user));
  }

  get userName(): string {
    if (!this.user) {
      return 'No username';
    }

    return `${this.user.firstName} ${this.user.lastName}`;
  }

  public handleLogoutButtonClick(): void {
    this.storageService.clear('token');
    this.authService.setUser(null);
    this.router.navigate(['auth', 'login']);
  }
}
