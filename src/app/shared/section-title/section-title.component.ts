import { Component } from '@angular/core';

@Component({
  selector: 'app-section-title',
  template: `
    <h2>
      <ng-content></ng-content>
    </h2>
  `,
  styleUrls: ['./section-title.component.scss']
})
export class SectionTitleComponent {}
