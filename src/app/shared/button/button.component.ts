import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
})
export class ButtonComponent implements OnInit {
  @Input('type') btnType: string;
  @Output('clicked') click: EventEmitter<MouseEvent> = new EventEmitter<
    MouseEvent
  >();

  ngOnInit() {
    if (!this.btnType) {
      this.btnType = 'button';
    }
  }

  public onClick = (e: MouseEvent): void => {
    this.click.emit(e);
  };
}
