import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { Router } from '@angular/router';

import { SidebarService } from '../services/sidebar.service';
import { AuthService } from '../../auth/services/auth.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit, OnDestroy {
  private sidebarShownSubscription: Subscription;
  private authSubscription: Subscription;
  private countSubscription: Subscription;
  public sidebarShown: boolean;
  public userRole: string;
  public count: number;

  constructor(
    private router: Router,
    private sidebarService: SidebarService,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.countSubscription = this.sidebarService.count$.subscribe(
      count => (this.count = count)
    );

    this.sidebarShownSubscription = this.sidebarService.sidebarShown$.subscribe(
      val => (this.sidebarShown = val)
    );

    this.authSubscription = this.authService.user$
      .pipe(take(1))
      .subscribe(user => (this.userRole = user.role));
  }

  ngOnDestroy() {
    this.countSubscription.unsubscribe();
    this.sidebarShownSubscription.unsubscribe();
    this.authSubscription.unsubscribe();
  }

  closeSidebar() {
    this.sidebarService.closeSidebar();
  }

  public handleInterviewsClick(): void {
    this.router.navigate(['director', 'interviews']);
  }

  public handleRequestsClick(): void {
    this.router.navigate(['recruiter', 'new-requests']);
  }
}
