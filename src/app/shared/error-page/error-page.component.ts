import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-error-page',
  templateUrl: './error-page.component.html',
  styleUrls: ['./error-page.component.scss']
})
export class ErrorPageComponent {
  @Input() errorCode: number = 400;
  @Input() errorMessage: string = 'Error Message';
  @Input() buttonText: string = 'Вернуться';
  @Output() clicked = new EventEmitter();

  public handleClick(): void {
    this.clicked.emit();
  }
}
