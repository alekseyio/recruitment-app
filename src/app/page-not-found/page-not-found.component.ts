import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html'
})
export class PageNotFoundComponent {
  public errorCode = 404;
  public errorMessage = 'Данная страница не существует';

  constructor(private router: Router, private route: ActivatedRoute) {}

  handleClick() {
    this.router.navigate(['..'], { relativeTo: this.route });
  }
}
