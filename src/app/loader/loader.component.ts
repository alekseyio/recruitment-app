import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { GlobalLoaderService } from '../services/global-loader.service';

@Component({
  selector: 'app-loader',
  template: `
    <div class="loader" *ngIf="isShown">
      <div class="loader__container">
        <img
          src="assets/img/icons/loader.svg"
          alt="Loading..."
          class="loader__image"
        />
      </div>
    </div>
  `,
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit, OnDestroy {
  private loaderSubscription: Subscription;
  public isShown = false;

  constructor(private loaderService: GlobalLoaderService) {}

  ngOnInit() {
    this.loaderSubscription = this.loaderService.shown$.subscribe(
      shown => (this.isShown = shown)
    );
  }

  ngOnDestroy() {
    this.loaderSubscription.unsubscribe();
  }
}
